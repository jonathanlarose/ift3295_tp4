#!/usr/bin/python -S
 # -*- coding: utf-8 -*-

#================================#
#== ift3295 - Bio-Informatique ==#
#== TP4                        ==#
#== par Jonathan Larose        ==#
#================================#


import numpy as np
from ete3 import Tree
import sys

# Argument en entrée
pathnw = sys.argv[1]
pathfasta = sys.argv[2]

#====================================================================
# BLOSUM62
blosum62 = np.loadtxt('BLOSUM62.txt', usecols=range(1,25), skiprows=1)
blosum_ref = ''.join(np.loadtxt('BLOSUM62.txt', dtype=str, usecols=0, skiprows=1))


#====================================================================
# FONCTIONS

# Fonction pour importer / parser un fichier .fasta
def parse_fasta(path):
    
    fastafile = np.loadtxt(path, dtype=str, delimiter=None)
    sequences = []
    sequences_id = []
    seq = ''

    for line in fastafile:

        if(line.startswith('>')):
            if(seq!= ''):
                sequences.append(seq)
            sequences_id.append(line.strip('>'))
            seq = ''
        else:
            seq = seq + line
    sequences.append(seq)
    
    return [sequences_id, sequences]


# Fonction pour retirer tous les gaps dans un alignement de sequences
def filter_gap(list_seq):
    
    matrix_seq = [list(s) for s in list_seq]
    
    # Pour chacune des sequences dans la liste
    for i in range(len(matrix_seq)):
        
        seq = matrix_seq[i]
        
        # Tant qu'il y a des indels
        while '-' in seq:
            pos = seq.index('-')
            
            #Retire dans toutes les sequences la positions correspondant à l'indel
            for s in matrix_seq:
                del s[pos]
    
    return [''.join(s) for s in matrix_seq]


# Fonction qui calcule et retourne la distance entre 2 séquences
def distance_sequence(s1, s2):
    
    p = 0
    q1 = 0
    q2 = 0
    
    for i in range(len(s1)):
        p += blosum_score(s1[i],s2[i])
        q1 += blosum_score(s1[i],s1[i])
        q2 += blosum_score(s2[i],s2[i])
    
    return 1-(p / max(q1, q2))


# Fonction qui retourne le poids associé à une substitution c1 vers c2
def blosum_score(c1, c2):
    
    l = blosum_ref.index(c1)
    c = blosum_ref.index(c2)
    
    return blosum62[l][c]


# Fonction qui crée un arbre binaire en fonction d'une matrice de score
def neighbor_joining(dist_matrix, nodes):
    
    while(len(nodes)>2):
        
        l = len(nodes)
        q_matrix = np.zeros((l,l))
        minimum = 10000
        node_i = ''
        node_j = ''
        
        # Calcul de la matrice de distance entre chaque pair de noeuds
        for i in range(l):
            for j in range(l):
                
                if(i==j):
                    continue
                
                dij = dist_matrix[i][j]
                ri = (np.sum(dist_matrix, axis=1)[i]-dij) / (l-2)
                rj = (np.sum(dist_matrix, axis=1)[j]-dij) / (l-2)
                q_matrix[i][j] = dij - (ri + rj)
                
                if(q_matrix[i][j] < minimum):
                    
                    minimum = q_matrix[i][j]
                    i_index = i
                    j_index = j
                    ri_min = ri
                    rj_min = rj
        
        
        # Nouvelle node à partir de la pair de node à distance minimal
        
        di = (dist_matrix[i_index][j_index] + (ri_min - rj_min)) / 2
        dj = dist_matrix[i_index][j_index] - di
        k = new_wick(nodes[i_index], nodes[j_index], di, dj)
        
        # distance au nouveau noeud
        
        dk = np.zeros(l+1)
        for m in range(l):
            dk[m+1] = (dist_matrix[i_index][m] + dist_matrix[j_index][m] - dist_matrix[i_index][j_index]) / 2
            
        dk = np.delete(dk, [i_index+1, j_index+1], axis=0)
        
        del_index = [i_index,j_index]
        
        # update matrice de distance et liste de node
        
        dist_matrix = np.delete(dist_matrix, del_index, axis=0)
        dist_matrix = np.delete(dist_matrix, del_index, axis=1)
        nodes = np.delete(nodes, del_index, axis=0)
        
        nodes = np.append(np.array(k), nodes)
        dist_matrix = np.insert(dist_matrix, 0, dk[1:], axis=0)
        dist_matrix = np.insert(dist_matrix, 0, dk, axis=1)
        
    
    # Quand il ne reste plus que 2 noeuds   
    
    nw = new_wick(nodes[0], nodes[1], dist_matrix[0][1], dist_matrix[0][1])
    
    return Tree(nw + ";", format=1)



#Fonction qui retourne un node en expression nw
def new_wick(A,B,w1,w2):
    
    return "(" + A + ":" + str(w1) + "," + B + ":" + str(w2) + ")"
    

# Fonction qui calcule la distance RF entre deux instances de la structure d'arbre
def distance_rf(t1, t2):
    
    bipart1 = bipartion(t1)
    bipart2 = bipartion(t2)
    bipart2_inv = [np.flip(b,0) for b in bipart2]
    
    inboth = [b for b in bipart1 if b in np.concatenate((bipart2, bipart2_inv))]
        
    return len(bipart1)-len(inboth) + len(bipart2)-len(inboth)


# Fonction qui retourne une liste des bipartitions dans un arbre
def bipartion(t):
    
    bipart_list = []
    leaves = t.get_leaf_names()
    nb = len(leaves)
    
    for n in t.traverse('postorder'):
        
        if len(n) > 1 and len(n) <= nb-2:
            
            cluster = n.get_leaf_names()
            cluster.sort()
            complement = [l for l in leaves if l not in cluster]
            complement.sort()
            bipart = [cluster, complement]
            
            # Si la bipartition n'est pas déjà dans la liste, on l'ajoute
            if (bipart not in bipart_list) and (list(np.flip(bipart,0)) not in bipart_list):
                bipart_list.append(bipart)
    
    return bipart_list


#====================================================================
# IMPORT

# import Newick file
nwfile = np.loadtxt(pathnw, dtype=str, delimiter='\n', unpack=True, usecols=0)
tree_list = []
for t in nwfile:
    tree_list.append(Tree(t))

# import sequences alignés .fasta
fasta = parse_fasta(pathfasta)
sequences_id = fasta[0]
sequences = fasta[1]


#====================================================================
# MANIPULATION

# Filtre les gaps dans les sequences alignés
sequences = filter_gap(sequences)

# association nom et sequences
dict_seq = dict(zip(sequences_id, sequences))
for t in tree_list:
    for leaf in t:
        leaf.add_features(sequence=dict_seq.get(leaf.name, "none"))

# Calcul de la matrice de distance entre les séquences à partir de l'alignement filtré
dist_matrix = np.zeros((len(sequences),len(sequences)))
for i in range(len(sequences)):
    for j in range(len(sequences)):    
        dist_matrix[i][j] = distance_sequence(sequences[i],sequences[j])

        
# Cree l'arbre correspondant à la matrice de distance avec l'algo de neighbor joining
t_nb = neighbor_joining(dist_matrix, sequences_id)

print "L'arbre NJ correspondant à l'alignement de séquences est le suivant :"

print t_nb

# Distance rf entre chacun des arbres en entrée
d_min = dist_matrix.shape[0] * 2
arbre_min_rf = []
index_best = 0

for i in range(len(tree_list)):
    
    rf = distance_rf(tree_list[i], t_nb)
    print "\n\nLa distance RF entre l'arbre NJ et l'arbre :\n\t" + nwfile[i] + "\nest de : " + str(rf)
    
    if rf < d_min:
        arbre_min_rf = [tree_list[i]]
        d_min = rf
        index_best = i

    
print "\nDonc l'arbre avec une distance RF minimal à l'arbre NJ est le " + str(index_best) + "e arbre :\n"
for t in arbre_min_rf:
    print t
    
print "\n\nAvec une distance RF de :" + str(d_min)

# Enracinement :
branch_max = 0
name = ''

leaves = t_nb.get_leaves()

for l in leaves:
    if(l.dist > branch_max):
        branch_max = l.dist
        name = l.name

print "\n\nL'enracinement de l'arbre se fait sur la plus longue branche qui est celle qui relie <" + name + "> au reste."
print "Sa taille est de : " + str(branch_max)